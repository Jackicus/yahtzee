/* each item in the score sheet can only be chosen once and
 * has a integer value, sometimes set sometimes calculated of
 * dice total */

public class Scoresheet_option {
	String scoreName;
	int score;
	boolean picked = false;

//___ ___ ___ ___ constructor to set string attribute ___
	public Scoresheet_option(String name) {
            scoreName = name;
	}

//////////////////////////////////////////////////////////
//          GET AND SET METHODS
//////////////////////////////////////////////////////////

//___ ___ ___ ___ SETS THE VALUE OF SCORE
	public void setScore(int points) {
            this.score = points;
	}

//___ ___ ___ ___ SETS THE OPTION AS PICKED
	public void setPicked() {
            this.picked = true;
	}

//___ ___ ___ ___ GETS THE SCORE VALUE	___ ___ ___ ___ ___
	public int getScore() {
            return this.score;
	}

//___ ___ ___ ___ GETS THE PICKED STATUS___ ___ ___ ___ ___
	public boolean getPicked() {
            return this.picked;
	}

}
