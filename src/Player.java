import java.util.Arrays;

public class Player {
	Die[] dice = { new Die(), new Die(), new Die(), new Die(), new Die() };
	Scoresheet scoresheet = new Scoresheet();

//////////////////////////////////////////////////////////
//		DICE[] METHODS
//////////////////////////////////////////////////////////

//___ ___ Action of throwing non kept dice	
	public void diceThrow() {
            for (int i = 0; i < dice.length; i++) {
		if (dice[i].keep == false) {
                    dice[i].roll();
		}
            }
	}
//___ ___ Reset all dice to 0 and false
	public void diceReset() {
            for (int i = 0; i < this.dice.length; i++) {
		this.dice[i].resetDie();
            }
        }
//___ ___ Return specific die       
	public int diceShow(int index) {
            return dice[index].result;
	}
//___ ___ Choose specific diced to skip roll	
        public void dieSave(int die) {
                dice[die].save();   
	}

//////////////////////////////////////////////////////////
//		SCORESHEET METHODS
//////////////////////////////////////////////////////////	
	
//___ ___ Return the potential result if option isnt picked
		public String availibleOption(int i) {
                    if (this.scoresheet.isPicked(i) == false) {
                        return Integer.toString(this.calculation(i+1));	
                    }else {
                        return "XXX";
                    }
		}	

//___ ___ Select Option
	public void chooseOption(int option) {
		if (this.scoresheet.isPicked(option) == false) {
			setOptionScore(option);
                        this.scoresheet.sheet[option].setPicked();
                }
        }

//___ ___ Option Selector
// the integer must match the score sheet option number
	public int calculation(int option) {
		switch (option) {
		case 1:
			return this.totalOf(1);
		case 2:
			return this.totalOf(2);
		case 3:
			return this.totalOf(3);
		case 4:
			return this.totalOf(4);
		case 5:
			return this.totalOf(5);
		case 6:
			return this.totalOf(6);
		case 7:
			return this.opt7();
		case 8:
			return this.opt8();
		case 9:
			return this.opt9();
		case 10:
			return this.opt10();
		case 11:
			return this.opt11();
		case 12:
			return this.opt12();
		case 13:
			return this.opt13();
		}
		return 0;
	}
	public void setOptionScore(int option) {
		switch (option) {
		case 1:
			this.scoresheet.setOnes(totalOf(1));
			break;
		case 2:
			this.scoresheet.setTwos(totalOf(2));
			break;
		case 3:
			this.scoresheet.setThrees(totalOf(3));
			break;
		case 4:
			this.scoresheet.setFours(totalOf(4));
			break;
		case 5:
			this.scoresheet.setFives(totalOf(5));
			break;
		case 6:
			this.scoresheet.setSixes(totalOf(6));
			break;
		case 7:
			this.scoresheet.setThreeOfAKind(opt7());
			break;
		case 8:
			this.scoresheet.setFourOfAKind(opt8());
			break;
		case 9:
			this.scoresheet.setFullHouse(opt9());
			break;
		case 10:
			this.scoresheet.setSmallStraight(opt10());
			break;
		case 11:
			this.scoresheet.setLongStraight(opt11());
			break;
		case 12:
			this.scoresheet.setTotalOfDice(opt12());
			break;
		case 13:
			this.scoresheet.setYahtzee(opt13());
			break;
		}
	}


//////////////////////////////////////////////////////////
//Below is all score rules that apply to a yahtzee player/
//These all use the players array of dice to work out    /
//each potential outcome for the subclass scoresheet     /
//////////////////////////////////////////////////////////


/* 01-06 */// TOTAL OF //////////////////////////////////
	public int totalOf(int num) {
		int score = 0;
		for (int i = 0; i < this.dice.length; i++) {
			if (this.dice[i].result == num) {
				score += num;
			}
		}
		return score;
	}

/* 07 */// THREE OF A KIND////////////////////////////
	public int opt7() {
		int counter = 0;
		int matchedNum = 0;

		for (int i = 0; i < this.dice.length; i++) { //Loop to select dice number
			if (counter >= 3) { //Check if right ammount of matches
				matchedNum = this.dice[i - 1].result;
			}
			counter = 0;
			int check = this.dice[i].result;
			for (int ii = 0; ii < this.dice.length; ii++) { //Loop to check selected dice number for matches
				if (this.dice[ii].result == check) {
					counter++;
				}
			}
		}
		return matchedNum * counter;
	}

/* 08 */// FOUR OF A KIND////////////////////////////////
	public int opt8() {
		int counter = 0;
		int matchedNum = 0;
		
		for (int i = 0; i < this.dice.length; i++) { //Loop to select dice number
			if (counter >= 4) { //Check if right amount of matches
				matchedNum = this.dice[i - 1].result;
			}
			counter = 0;
			int check = this.dice[i].result;
			for (int ii = 0; ii < this.dice.length; ii++) { //Loop to check selected dice number for matches
				if (this.dice[ii].result == check) {
					counter++;
				}
			}
		}
		return matchedNum * counter;
	}

/* 09 */// FULL HOUSE///////////////////////////////////
	public int opt9() {
		int counter = 0;
		String indexCounter = "";
		int counterTwo = 0;

		for (int i = 0; i < this.dice.length; i++) { 									// Loop to select dice number
			if (counter >= 3) { 															// Check if right 3 matches present then check for 2
				for (int iii = 0; iii < this.dice.length; iii++) { 								// Check for pair but don't repeat already searched index of the three of a kind.
					if (indexCounter.contains(Integer.toString(iii)) == false) { 					// skip TOK indexes they are useless
						int check = this.dice[iii].result; 												// set check
						for (int iiii = 0; iiii < this.dice.length; iiii++) { 							// check against check loop
							if (indexCounter.contains(Integer.toString(iii)) == false) { 					// skip TOK indexes again they are useless now
								if (this.dice[iiii].result == check) { 											// if check is a match count it
									counterTwo++;
									if (counterTwo == 2) { 															// if check reaches 2 we have our pair to our TOK return score
										return 25;
									}
								}
							}
						}
					}
					counterTwo = 0;
				}
			}
			counter = 0;
			indexCounter = "";
			int check = this.dice[i].result;
			for (int ii = 0; ii < this.dice.length; ii++) { //Loop to check selected dice number for matches
				if (this.dice[ii].result == check) {
					indexCounter = indexCounter.concat(Integer.toString(ii));
					counter++;
				}
			}
		}
		return 0;
	}

/* 10 */// SMALL STRAIGHT////////////////////////////////
	public int opt10() {
		int counter = 1;

		int[] sortedIntDiceList = { this.dice[0].result, this.dice[1].result, this.dice[2].result, this.dice[3].result,
				this.dice[4].result }; 						// convert dice to INT array for sorting and sort
		Arrays.sort(sortedIntDiceList); 						// start with first integer to check
		int check = sortedIntDiceList[0]; 						// loop starting with dice 2 (we already set 1 as our check)
		for (int i = 1; i < this.dice.length; i++) { 			// we are checking if the dice ascends by one 3 time in a row
			if (check + 1 == sortedIntDiceList[i]) { 				// counter to track times in a row
				counter++; 												// if 3 times in a row return score
				if (counter >= 4) {
					return 30;
				}
			} else { // if we dont have a match from the first "if" we reset the counter to 1 (we
						// count from one) unless it's a dupliacte because of ordering a duplicate would
						// reset the counter

				if (check != sortedIntDiceList[i]) { // end of each loop we set the the next check to be checked
					counter = 1;
				}
			}

			check = sortedIntDiceList[i];
		}
		return 0;
	}

	/* 11 */// LONG STRAIGHT////////////////////////////////
	public int opt11() {
//almost the same as small straight
		int counter = 1;
		int[] sortedIntDiceList = { this.dice[0].result, this.dice[1].result, this.dice[2].result, this.dice[3].result,
				this.dice[4].result };
		Arrays.sort(sortedIntDiceList);
		int check = sortedIntDiceList[0];
		for (int i = 1; i < this.dice.length; i++) {
			if (check + 1 == sortedIntDiceList[i]) {
				counter++;
				if (counter >= 5) {
					return 40;
				}
			} else {
				if (check != sortedIntDiceList[i]) {
					counter = 1;
				}
			}
			check = sortedIntDiceList[i];
		}
		return 0;
	}

/* 12 */// Total Of Dice////////////////////////////////
	public int opt12() {
		return this.dice[0].result + this.dice[1].result + this.dice[2].result + this.dice[3].result
				+ this.dice[4].result;
	}

/* 13 */// YAHTZEEEEEEEEE////////////////////////////////
	public int opt13() {
		int count = 0;
		for (int i = 0; i < this.dice.length; i++) {
			if (this.dice[i].result == this.dice[0].result) {
				count++;
			}
			if (count == 5) {
				return 50;
			}
		}
		return 0;

	}
//BELOW METHODS ARE USED IN THE CALCULATIONS ONLY
//___ ___ ___ DICE toString	___ ___ ___ ___ ___
	public String toString() {
		String nums = "";
		for (int i = 0; i < this.dice.length; i++) {
			nums.concat(this.dice[i].toString() + "\t");
		}
		return nums;
	}

//___ ___ ___DICE index	___ ___ ___ ___ ___ ___
	public int diceNumber(int index) {
		return this.dice[index].result;
	}

//___ ___ ___ SORT DICE (CHANGES INDEX) ___ ___
	public Player sortDice() {
		int[] list = { dice[0].result, dice[1].result, dice[2].result, dice[3].result, dice[4].result };
		Arrays.sort(list);
		for (int i = 0; i < dice.length; i++) {
			dice[i].result = list[i];
		}
		return this;
	}

}
