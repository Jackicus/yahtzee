/* The Score sheet for Yahtzee has 13 sections that 
 * can only be picked once and equal a integer value*/

public class Scoresheet {
	Scoresheet_option[] sheet = { 
	/*01*/	new Scoresheet_option("1. Ones"),
	/*02*/	new Scoresheet_option("2. Twos"),
	/*03*/	new Scoresheet_option("3. Threes"),
	/*04*/	new Scoresheet_option("4. Fours"),
	/*05*/	new Scoresheet_option("5. Fives"),
	/*06*/	new Scoresheet_option("6. Sixes"),
		//LOWER HALF
	/*07*/	new Scoresheet_option("7. Three of a kind"),
	/*08*/	new Scoresheet_option("8. Four of a kind"),
	/*09*/	new Scoresheet_option("9. Full House"),
	/*10*/	new Scoresheet_option("10. Small Straight"),
	/*11*/	new Scoresheet_option("11. Long Straight"),
	/*12*/	new Scoresheet_option("12. Total Of Dice"),
	/*13*/	new Scoresheet_option("13. Yahtzee!")
	};



	//___ ___ ___ ___ Check certain option is picked
	public boolean isPicked(int index) {
		if (sheet[index].getPicked()) {
			return true;
		}else {
			return false;
		}
	}

//////////////////////////////////////////////////////////
//		GET AND SET METHODS
//////////////////////////////////////////////////////////
	
        public String getOptScore(int optionIndex){
            if (this.sheet[optionIndex].getPicked() == true){
                return Integer.toString(this.sheet[optionIndex].getScore());
            }else{
                return "";
            }
        }

	public void setOnes(int points) {
		this.sheet[0].setScore(points);
		this.sheet[0].picked = true;
	}

	public int getOnes() {
		return this.sheet[0].getScore();
	}

	public void setTwos(int points) {
		this.sheet[1].setScore(points);
		this.sheet[1].setPicked();
	}

	public int getTwos() {
		return this.sheet[1].getScore();
	}

	public void setThrees(int points) {
		this.sheet[2].setScore(points);
		this.sheet[2].setPicked();
	}

	public int getThrees() {
		return this.sheet[2].getScore();
	}

	public void setFours(int points) {
		this.sheet[3].setScore(points);
		this.sheet[3].setPicked();
	}

	public int getFours() {
		return this.sheet[3].getScore();
	}

	public void setFives(int points) {
		this.sheet[4].setScore(points);
		this.sheet[4].setPicked();
	}

	public int getFives() {
		return this.sheet[4].getScore();
	}

	public void setSixes(int points) {
		this.sheet[5].setScore(points);
		this.sheet[5].setPicked();
	}

	public int getSixes() {
		return this.sheet[5].getScore();
	}

	public void setThreeOfAKind(int points) {
		this.sheet[6].setScore(points);
		this.sheet[6].setPicked();
	}

	public int getThreeOfAKind() {
		return this.sheet[6].getScore();
	}

	public void setFourOfAKind(int points) {
		this.sheet[7].setScore(points);
		this.sheet[7].setPicked();
	}

	public int getFourOfAKind() {
		return this.sheet[7].getScore();
	}

	public void setFullHouse(int points) {
		this.sheet[8].setScore(points);
		this.sheet[8].setPicked();
	}

	public int getFullHouse() {
		return this.sheet[8].getScore();
	}

	public void setSmallStraight(int points) {
		this.sheet[9].setScore(points);
		this.sheet[9].setPicked();
	}

	public int getSmallStraight() {
		return this.sheet[9].getScore();
	}

	public void setLongStraight(int points) {
		this.sheet[10].setScore(points);
		this.sheet[10].setPicked();
	}

	public int getLongStraight() {
		return this.sheet[10].getScore();
	}

	public void setTotalOfDice(int points) {
		this.sheet[11].setScore(points);
		this.sheet[11].setPicked();
	}

	public int getTotalOfDice() {
		return this.sheet[11].getScore();
	}

	public void setYahtzee(int points) {
		this.sheet[12].setScore(points);
		this.sheet[12].setPicked();
	}

	public int getYahtzee() {
		return this.sheet[12].getScore();
	}
}