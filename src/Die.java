import java.util.Random;

public class Die {
//---this is a single dice object 1 - 6 ----//	
	
	//Dice top value
	int result;
	//T/F if dice is being kept
	boolean keep;
//------------------------------------------//	
//---Dice ROLL Action-----------------------//
	public Die roll() {
		if (this.keep == false) {
			Random roll = new Random();
			this.result = roll.nextInt(6) + 1;
			return this;
		}else {
			return this;
		}
	}

//------------------------------------------//
//---Dice toString-----------------------------//	
	public String toString() {
		String num = Integer.toString(this.result);
		return num;
	}
//------------------------------------------//
//---Toggle Die Keep------------------------//
	public void save() {
		if (this.keep == false) {
			this.keep = true;
		}
		else {
			this.keep = false;
		}
	}
//------------------------------------------//
//---Dice reset ----------------------------//
	public void resetDie() {
		this.keep = false;
		this.result = 0;
	}
	
	
//------------------------------------------//
//---Loop for throw of multiple dice--------//
//	public static void rollMultiple(Die[] dice) {
//		for (int i = 0; i < dice.length; i++) {
//			if (dice[i].keep == false) {
//				dice[i].roll();
//			}
//		}
//	}
}